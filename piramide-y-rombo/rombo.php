<?php

$rows = 30;

for($i = 1; $i <= $rows; $i++) {
  echo '<div style="display: flex; justify-content: center;">';
  
  if($i < ($rows / 2 + 1)) {
    for($j = 0; $j < $i; $j++) {
      echo '*';
    }
  } else {
    for($j = 0; $j < ($rows - $i); $j++) {
      echo '*';
    }
  }
  
  echo '</div>';
}

?>
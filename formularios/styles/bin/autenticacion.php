<?php
include('./sesion.php');

foreach($_SESSION['user'] as $key => $user) {
  if($user['account_num'] == $_POST['account_num'] && $user['password'] == $_POST['password']) {
    header('location: ../info.php?id='.$key);
    exit;
  }
}

header('location: ../login.php?usuario_invalido=si');
?>
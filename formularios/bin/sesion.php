<?php
session_start();

// Inicializamos la sesion con un usuario predeterminado
// en caso de estar vacio
if(empty($_SESSION['user'])) {
  $_SESSION['user'] =  [
    [
      'account_num' => '1',
      'name' => 'Admin',
      'surname' => 'General',
      'second_surname' => '',
      'password' => 'adminpass123.',
      'gender' => 'O',
      'birth_date'=> '05/11/2001'
    ],
  ];
}

?>
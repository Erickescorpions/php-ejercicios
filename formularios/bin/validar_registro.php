<?php

session_start();

$id = $_GET['id'];

// Se valida que los datos del formulario para registrar un alumno
// no esten vacios
if($_POST['password'] == "" || $_POST['account_num'] == "" || $_POST['name'] == "") {
  header('location: ../formulario.php?error=true&id='.$id);
  exit;
} else {

  // Se valida que el numero de cuenta ingresado sea valido 
  $account_num = $_POST['account_num'];

  foreach($_SESSION['user'] as $key => $data) {
    if($account_num == $data['account_num']) {
      header('location: ../formulario.php?accountNumInvalid=true&id='.$id);
      exit;
    }
  }

  // Si el formulario es validado correctamente se crea un nuevo usuario
  $index = key(end($_SESSION)) + 1;
  array_push($_SESSION['user'], [
    'account_num' => $account_num,
    'name' => $_POST['name'],
    'surname' => $_POST['surname'],
    'second_surname' => $_POST['second_surname'],
    'password' => $_POST['password'],
    'gender' => $_POST['gender'],
    'birth_date'=> date("d/m/Y", strtotime($_POST['birth_date']))
  ]);

  header('location: ../info.php?id='.$id);
}

?> 
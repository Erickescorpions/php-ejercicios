<?php

/*
  Como mecanismo para limitar el acceso al sistema sin autenticarse
  pase un valor mediante get con un id para el usuario, para esta 
  implementacion solo se trata de la posicion en la que se encuentra
  guardado este dato dentro de $_SESSION['user']
*/
if(!isset($_GET['id'])) {
  header("location: ./login.php?autenticacion=no");
}

session_start();
$id = $_GET['id'];
$user = $_SESSION['user'][$id];

$name = $user['name'] . " " . $user['surname'] . " " . $user['second_surname'];
$account_num = $user['account_num'];
$birth_date = $user['birth_date'];

?>


<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Info</title>
    <link rel="stylesheet" type="text/css" href="styles/info.css">
  </head>
  <body>
    <nav>
      <ul>
        <li><a href="#" class="active">Home</a></li>
        <li><?php echo('<a href="./formulario.php?id='.$id.'">Registrar Alumnos</a>')?></li>
        <li><?php echo('<a href="./login.php">Cerrar Sesion</a></li>') ?>
      </ul>
    </nav>

    <div id="main-container">
      <h2>Usuario Autenticado</h2>
      <section id="info-container">
      
        <div id="name-container"> <h2> <?php echo($name); ?> </h2> </div>
        <div id="data-container">
          <span>Informacion</span>
          <span>Numero de cuenta: <?php echo($account_num); ?> </span>
          <span>Fecha de nacimiento: <?php echo($birth_date); ?> </span>
        </div>
      </section>


      <h2>Datos guardados:</h2>
      <section id="table-container">
        <div class="box"># cuenta</div>
        <div class="box">Nombre</div>
        <div class="box">Fecha de nacimiento</div>

        <?php
        foreach($_SESSION['user'] as $user) {
          echo('<div class="box">'.$user['account_num'].'</div>');
          echo('<div class="box">'.$user['name'] . " " . $user['surname']. " " . $user['second_surname'] . '</div>');
          echo('<div class="box">'.$user['birth_date'].'</div>');
        }
        ?>
      </section>
    </div>
  </body>
</html>
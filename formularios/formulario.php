<?php

if(!isset($_GET['id'])) {
  header("location: ./login.php?autenticacion=no");
}

session_start();
$id = $_GET['id'];

?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Registrar Alumnos</title>
    <link rel="stylesheet" type="text/css" href="styles/formulario.css">
  </head>
  <body>
    <nav>
      <ul>
        <li><?php echo('<a href="./info.php?id='.$id.'">Home</a>'); ?> </li>
        <li><a href="#" class="active">Registrar Alumnos</a></li>
        <li><a href="./login.php">Cerrar Sesion</a></li>
      </ul>
    </nav>

    <?php if(isset($_GET['error'])): ?>
      <?php if($_GET['error'] == true): ?>
        <div id="error-container">Necesitas llenar al menos los campos: 'Numero de cuenta', 'Nombre' y 'Contraseña'</div>
      <?php endif; ?>
    <?php endif; ?>

    <?php if(isset($_GET['accountNumInvalid'])): ?>
      <?php if($_GET['accountNumInvalid'] == true): ?>
        <div id="error-container">El numero de cuenta ingresado ya esta en uso.</div>
      <?php endif; ?>
    <?php endif; ?>

    <div id="main-container">
      <form <?php echo('action="./bin/validar_registro.php?id='.$id.'"'); ?> method="post" enctype="multipart/form-data" id="form-container">
        
        <label for="account-num">Numero de cuenta</label>
        <input type="text" id="account-num" name="account_num">

        <label for="name">Nombre</label>
        <input type="text" id="name" name="name">
        
        <label for="surname">Primer Apellido</label>
        <input type="text" id="surname" name="surname">
        
        <label for="second-surname">Segundo Apellido</label>
        <input type="text" id="second-surname" name="second_surname">

        <label for="gender" id="gender">Genero</label>
          <div id="hombre-container">
            <input type="radio" id="hombre" name="gender" value="H">
            <label for="hombre" id="hombre-label">Hombre</label>
          </div>
          <div id="mujer-container">
            <input type="radio" id="mujer" name="gender" value="M">
            <label for="mujer">Mujer</label>
          </div>
          <div id="otro-container">
            <input type="radio" id="otro" name="gender" value="O">
            <label for="otro">Otro</label>
          </div>

        <label for="birth-date">Fecha de nacimiento</label>
        <input type="date" id="birth-date" name="birth_date">
        <label for="password">Contraseña</label>
        <input type="password" id="password" name="password">

        <input type="submit" value="Registrar">
      </form>
    </div>
  </body>
</html>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="styles/login.css">
  </head>
  <body>
    <div id="main-container">
      <header id="title">
        <h1>Login</h1>
      </header>
      <main id="form-container">
        <form action="./bin/autenticacion.php" method="post" enctype="multipart/form-data">
          <label for="account-num">Numero de cuenta</label>
          <input type="text" id="account-num" name="account_num">
          <label for="password">Contraseña</label>
          <input type="password" id="password" name="password">
          <input type="submit" id="btn-submit" name="btn_submit">
        </form>
      </main>
    </div>

    <!--
      Para tener respuesta de la autenticacion 
      se recibe un valor mediante GET  en caso 
      de existir dicho valor y tener el valor 
      esperado se muestra el contenido dentro 
      del if. 
    -->
    <?php if(isset($_GET['usuario_invalido'])): ?>
      <?php if($_GET['usuario_invalido'] == 'si'): ?>
        <div id="error-container"> Numero de cuenta o contraseña incorrectos </div>
      <?php endif; ?>
    <?php endif; ?>
    <?php if(isset($_GET['autenticacion'])): ?>
      <?php if($_GET['autenticacion'] == 'no'): ?>
        <div id="error-container"> Necesitas auntenticarte para acceder al sistema </div>
      <?php endif; ?>
    <?php endif; ?>

  </body>
</html>
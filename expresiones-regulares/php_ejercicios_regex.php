<?php

function printMsg($str, $val) {
  if($val) {
    echo $str . " es valido. <br>"; 
  } else {
    echo $str . " es invalido. <br>";
  }
} 

//Realizar una expresión regular que detecte emails correctos.

echo 'Pruebas email <br><br>';

$email = "erickescorpions@hotmail.com"; // email correcto
$invalid_email = "erick1213.hotmail";
$invalid_email2 = "erick@gmail.com";

$email_exp = '/([a-zA-Z0-9.])+(\@)([a-z]+)(\.com|\.es|\.mx)/';

printMsg($email, preg_match($email_exp, $email));
printMsg($invalid_email, preg_match($email_exp, $invalid_email));
printMsg($invalid_email2, preg_match($email_exp, $invalid_email2));


//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.

echo '<br> Pruebas curp <br><br>';

$curp = "VASE011105HDFZNRA6";
$invalid_curp = "VASE011105HDFZNRA67";

// el curp tiene que tener una longitud de 18
$curp_exp = '/^[A-Z]{4}[0-9]{6}[A-Z]{7}[0-9]{1}$/';
printMsg($curp, preg_match($curp_exp, $curp));
printMsg($invalid_curp, preg_match($curp_exp, $invalid_curp));


//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.

echo '<br> Pruebas palabra longitud mayor 50 <br><br>';

$word = "asdfghjklpoiuqwewqpeoiqweowpqeiqwopeipqweiwqpewpqeqwpoqwieqow";
$invalid_word = "asdfghjkl poiuqwewqpeoiqweowpqe iqwopeipqweiwqpew pqeqwpoqwieqow";

$word50_exp = '/[A-Za-z]{50}[A-Za-z]+/';

printMsg($word, preg_match($word50_exp, $word));
printMsg($invalid_word, preg_match($word50_exp, $invalid_word));

//Crea una funcion para escapar los simbolos especiales.

echo '<br> Pruebas escape de caracteres especiales <br><br>';

$word = " Hola   mundo @";

$str = '${2}';
$symbols_exp = '/[^A-Za-z0-9]/';

$result = preg_replace($symbols_exp, $str ,$word);

echo $result;

//Crear una expresion regular para detectar números decimales.

echo '<br> Pruebas de numeros decimales <br><br>';

$num = "11.3";
$invalid_num = "11.";

$exp_dec =  '/\d*\.\d+/';

printMsg($num, preg_match($exp_dec, $num));
printMsg($invalid_num, preg_match($exp_dec, $invalid_num));

?>
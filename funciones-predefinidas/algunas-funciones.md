# Funciones predefiinidas PHP

1. substr: devuelve una parte del string definida por los parámetros start y length.
```
substr(string $string, int $start, int $length = ?): string
```

```
  <?php
  $rest = substr("abcdef", -1);    // devuelve "f"
  $rest = substr("abcdef", -2);    // devuelve "ef"
  $rest = substr("abcdef", -3, 1); // devuelve "d"
  ?>
```

2. strstr: encuentra la primera aparición de un string.

```
strstr(string $haystack, mixed $needle, bool $before_needle = false): string
```

haystack: El string en donde buscar.

needle: Cadena que se va a buscar. 

before_needle: Si se define como true, strstr() devolverá la parte del haystack antes de la primera ocurrencia de needle.

```
<?php
$email  = 'name@example.com';
$domain = strstr($email, '@');
echo $domain; // mostrará @example.com

$user = strstr($email, '@', true); // Desde PHP 5.3.0
echo $user; // mostrará name
?>
```

3. strpos: encuentra la posición de la primera ocurrencia de un substring en un string

```
strpos(string $haystack, mixed $needle, int $offset = 0): mixed
```

offset: Si se específica, la búsqueda iniciará en éste número de caracteres contados desde el inicio del string. 

4. implode: Une elementos de un array en un string

```
implode(string $separator, array $array): string
```


5. explode: Divide un string en varios string

```
explode(string $delimiter, string $string, int $limit = PHP_INT_MAX): array
```

6. utf8_encode: Convierte una cadena de ISO-8859-1 a UTF-8

7. utf8_decode: Convierte un string desde UTF-8 a ISO-8859-1, sustituyendo los caracteres no válidos o no representables

8. array_walk: aplica una función proporcionada por el usuario a cada miembro de un array

```
array_walk(array &$array, callable $callback, mixed $userdata = null): bool
```

9. serialize: Genera una representación apta para el almacenamiento de un valor
